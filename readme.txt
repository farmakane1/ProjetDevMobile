--ABOUT THE PROJECT--
Pour mettre en place le projet:
    -il faut d'abord créer une application ionic 
    -Créer une application strapi
    
  et ce sera dans cette aplication que l'on créeras nos différents collections: Reunion, Salle de reunion, employés 
    - On va définir les roles et permissions (créer, modifier, supprimer, afficher) de chaque utilisateur sur les collections  dans les paramétres.
    - On va définir la constante api_url pour définir l'environnement dans lequel s'exécute notre application et nous permettre de 
	récupérer des données des collections dans notre projet strapi api_url:http://localhost:1337.
  Les collections reunion et salle de reunion sont liees (la creation d'une reunion implique le choix d'une salle)

Le Projet devra permettre à l'utilisateur une fois au niveau de la salle de réunion de créer une réunion. 
La réunion peut être de deux types : 
-Réunion externe( pas de Qr code pour les externes) réunissant des internes et des externes
-Réunion interne. 
Les réunions externes sont déterminés par l'enregistrement des participants par formulaire
Les réunions internes par un scan Code QR.
	On pourra modifier les données d'une réunion, en supprimer une ou afficher la liste des réunions.
	De meme on pourra ajouter un participant, les modifier ou en supprimer.
