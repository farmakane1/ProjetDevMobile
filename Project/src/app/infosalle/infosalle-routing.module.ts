import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InfosallePage } from './infosalle.page';

const routes: Routes = [
  {
    path: '',
    component: InfosallePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InfosallePageRoutingModule {}
