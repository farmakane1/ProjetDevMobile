import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InfosallePageRoutingModule } from './infosalle-routing.module';

import { InfosallePage } from './infosalle.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InfosallePageRoutingModule
  ],
  declarations: [InfosallePage]
})
export class InfosallePageModule {}
