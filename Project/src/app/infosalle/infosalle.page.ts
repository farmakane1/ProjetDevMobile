import { Component, getModuleFactory, OnInit } from '@angular/core';
import { Reunions } from '../models/reunions';
import { ReunionsService } from '../services/reunions.service';
import { LoadingController, ToastController } from '@ionic/angular';
import { ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-infosalle',
  templateUrl: './infosalle.page.html',
  styleUrls: ['./infosalle.page.scss'],
})
export class InfosallePage implements OnInit {
  reunion: Reunions[];
  meet:Reunions;
  id:number;
  // salle:Salles;
  constructor(
    private service: ReunionsService, 
    private loadingController: LoadingController,
    private currentRoute:ActivatedRoute
  ) { 
    this.loadDatas();
     this.id=Number.parseInt(this.currentRoute.snapshot.paramMap.get('id'),10);
  //   // this.salle=this.currentRoute.snapshot.paramMap.getAll();
    //  let i=0;
    
  //   for (let reunion of this.service.reunion){
    //  if(this.reunion[0].salle['id']!=this.id){
    //    this.reunion=null;
    //   ;
       
    //  }
  //   //  i++;
  //  }
  
    
  }

  ngOnInit() {
  }

  private async loadDatas() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Chargement...',
    });
    await loading.present();
    this.service.getAll().subscribe((reunion)=>{
      //cas succes
       this.service.reunion= this.reunion = reunion;
      //  this.service.reunion= this.reunion = reunion;
       
      loading.dismiss();
      //to access it this.reunion[0].salle['nom']

      // if(this.id==this.reunion[i].salle['id']){
      //   console.log(this.reunion[i]);
      //   console.log(this.id+" valeur de i="+i);
      //   i++;
      // }
       for (let i = 0; i < this.reunion.length; i++) {
        if(this.id!=this.reunion[i].salle['id']){
        console.log(this.reunion[i].id+" in it "+this.reunion[i].OrdreduJour);
        continue;
        // this.reunion = reunion;  
        }
      }
 
   }, (error)=>{
     //echec
      console.log(error);
   });
  }

  ionViewDidEnter() {
    // this.loadDatas();
  }

}
