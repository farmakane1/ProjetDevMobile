import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'create',
        loadChildren: () => import('../create/create.module').then(m => m.createPageModule)
      },
      {
        path: 'read',
        loadChildren: () => import('../read/read.module').then(m => m.readPageModule)
      },
      {
        path: 'update/:id/:nom',
        loadChildren: () => import('../update/update.module').then(m => m.updatePageModule)
      },
      {
        path: 'delete',
        loadChildren: () => import('../delete/delete.module').then(m => m.DeletePageModule)
      },
      {
        path: 'home',
        loadChildren: () => import('../home/home.module').then(m => m.HomePageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/home',
        pathMatch: 'full'
      },
      {
        path: 'infosalle/:id/:nom',
        loadChildren: () => import('../infosalle/infosalle.module').then( m => m.InfosallePageModule)
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
