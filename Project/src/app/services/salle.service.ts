import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_URL } from '../../environments/environment';
import {Salles} from '../models/salle';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class SalleService {
  salle:Salles[];
  constructor(
    private httpClient:HttpClient
  ) { }
  getAll(): Observable<Salles[]> {
    return this.httpClient.get<Salles[]>(`${API_URL}/Salles`);
  }

  get(id: number): Observable<Salles> {
    return this.httpClient.get<Salles>(`${API_URL}/Salles/${id}`);
  }

  post(Salle: Salles): Observable<Salles> {
    return this.httpClient.post<Salles>(`${API_URL}/Salles/`, Salle);
  }

  put(Salle: Salles): Observable<Salles> {
    return this.httpClient.put<Salles>(`${API_URL}/Salles/${Salle.id}`, Salle);

  }

  delete(id: number): Observable<Salles> {
    return this.httpClient.delete<Salles>(`${API_URL}/Salles/${id}`);
  }
}
