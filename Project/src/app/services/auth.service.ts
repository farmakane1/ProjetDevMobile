import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { API_URL } from '../../environments/environment';
import {User} from '../models/user';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private httpClient: HttpClient
  ) { }
  login(user:User):Observable<any>{

    return this.httpClient.post<any>(`${API_URL}/auth/local`,user);
  }
}
