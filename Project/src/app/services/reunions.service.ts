import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_URL } from '../../environments/environment';
import {Reunions} from '../models/reunions';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ReunionsService {
  reunion:Reunions[];
  constructor(
    private httpClient:HttpClient
  ) { }
  getAll(): Observable<Reunions[]> {
    return this.httpClient.get<Reunions[]>(`${API_URL}/Reunions`);
  }

  get(id: number): Observable<Reunions> {
    return this.httpClient.get<Reunions>(`${API_URL}/Reunions/${id}`);
  }

  post(Salle: Reunions): Observable<Reunions> {
    return this.httpClient.post<Reunions>(`${API_URL}/Reunions/`, Salle);
  }

  put(Salle: Reunions): Observable<Reunions> {
    return this.httpClient.put<Reunions>(`${API_URL}/Reunions/${Salle.id}`, Salle);

  }

  delete(id: number): Observable<Reunions> {
    return this.httpClient.delete<Reunions>(`${API_URL}/Reunions/${id}`);
  }
}
