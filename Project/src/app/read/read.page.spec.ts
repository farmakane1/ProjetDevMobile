import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { readPage } from './read.page';

describe('readPage', () => {
  let component: readPage;
  let fixture: ComponentFixture<readPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [readPage],
      imports: [IonicModule.forRoot(), ExploreContainerComponentModule]
    }).compileComponents();

    fixture = TestBed.createComponent(readPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
