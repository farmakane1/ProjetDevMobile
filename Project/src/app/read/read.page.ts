import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UtilsService } from '../utils/utils.service';
import { Salles } from '../models/salle';
import { LoadingController, ToastController } from '@ionic/angular';
import { SalleService } from '../services/salle.service';
import {DELETE_WITH_SUCCESS,ERROR_MESSAGE, DANGER,SUCCESS} from '../constant';
@Component({
  selector: 'app-read',
  templateUrl: 'read.page.html',
  styleUrls: ['read.page.scss']
})
export class readPage {
  salle: Salles[];
  constructor(
    private router: Router,
    private service: SalleService,
    private utils: UtilsService,
    private loadingController: LoadingController,
    private toastCtrl: ToastController
  ) {
    this.loadDatas();
  }
  private async loadDatas() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Chargement...',
    });
    await loading.present();

    this.service.getAll().subscribe((salle)=>{
      //cas succes
      this.service.salle= this.salle = salle;
      loading.dismiss();
      // console.log(salle);
   }, (error)=>{
     //echec
      console.log(error);
   });
  }

  ionViewDidEnter() {
    this.loadDatas();
  }


  async delete(id: number) {
    const toast = await this.toastCtrl.create({
      message: 'Suppression avec succès.',
      duration: 2000
    });

    this.service.delete(id).subscribe(()=>{
        this.loadDatas();
        toast.present();
        this.utils.presentToast(DELETE_WITH_SUCCESS, SUCCESS, 2000);
    }, ()=>{
      this.utils.presentToast(ERROR_MESSAGE, DANGER, 2000);

    })
  }
  updateLoad(id:number,nom:string){
    this.router.navigate(['/tabs/update',id,nom]);
  }
  showInfo(id:number,nom:string){
    this.router.navigate(['/tabs/infosalle',id,nom])
  }

}
