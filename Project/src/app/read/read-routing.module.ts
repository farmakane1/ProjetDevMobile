import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { readPage } from './read.page';

const routes: Routes = [
  {
    path: '',
    component: readPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class readPageRoutingModule {}
