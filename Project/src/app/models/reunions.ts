export class Reunions {
    id: number;
    date: string;
    heureDebut: string;
    heureFin: string;
    OrdreduJour: string;
    salle:string;
}
