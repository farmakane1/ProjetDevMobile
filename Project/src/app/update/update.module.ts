import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { updatePage } from './update.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { updatePageRoutingModule } from './update-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    ReactiveFormsModule,
    ExploreContainerComponentModule,
    RouterModule.forChild([{ path: '', component: updatePage }]),
    updatePageRoutingModule,
  ],
  declarations: [updatePage]
})
export class updatePageModule {}
