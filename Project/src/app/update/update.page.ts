import { Component,OnInit} from '@angular/core';
import { FormBuilder,FormControl,FormGroup,Validators} from '@angular/forms';
import {SalleService} from '../services/salle.service';
import {DANGER,ERROR_MESSAGE,SUCCESS,UPDATE_WITH_SUCCESS } from '../constant';
import {UtilsService} from '../utils/utils.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Salles } from '../models/salle';
@Component({
  selector: 'app-update',
  templateUrl: 'update.page.html',
  styleUrls: ['update.page.scss']
})
export class updatePage implements OnInit{
  updateForm: FormGroup;
  id: number;
  salle: Salles;
  constructor(
    private fb: FormBuilder,
    private router:Router,
    private service:SalleService,
    private utils: UtilsService,
    private currentRoute:ActivatedRoute,
    
  ) {
    this.id=Number.parseInt(this.currentRoute.snapshot.paramMap.get('id'),10);

    this.updateForm= this.fb.group({
      nom: new FormControl('',[Validators.required]),
      description: new FormControl('',[Validators.nullValidator])
     })
  }
  ngOnInit(){

  }
  update(){
    let salle = this.updateForm.value;
    salle.id = this.id;
    this.service.put(salle).subscribe(()=>{
      this.utils.presentToast(UPDATE_WITH_SUCCESS,SUCCESS,2000);
      this.router.navigateByUrl('/tabs/read');
    }),()=>{
      this.utils.presentToast(ERROR_MESSAGE,DANGER,2000);
    }
  }

}
