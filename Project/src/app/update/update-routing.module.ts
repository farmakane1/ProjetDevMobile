import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { updatePage } from './update.page';

const routes: Routes = [
  {
    path: '',
    component: updatePage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class updatePageRoutingModule {}
