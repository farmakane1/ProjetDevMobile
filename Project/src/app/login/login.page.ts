import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { TOKEN } from '../constant';
import { AuthService } from '../services/auth.service';
import { UtilsService } from '../utils/utils.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private service: AuthService,
    private utils: UtilsService,
    private router:Router
  ) {
    this.loginForm = this.fb.group({
      identifier: new FormControl('',[Validators.required]),
      password: new FormControl('',[Validators.required])
    })
   }

  ngOnInit() {
  }
  login():void{
    this.service.login(this.loginForm.value).subscribe((user)=>{
      console.log(user);
      window.localStorage.setItem(TOKEN,user.jwt);
      this.router.navigateByUrl('SalleManage');
    },()=>{
        this.utils.presentToast('Identifiant ou mdp incorrect','danger');
    })
  }
}
