import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree,Router } from '@angular/router';
import { Observable } from 'rxjs';
import { TOKEN } from '../constant';

@Injectable({
  providedIn: 'root'
})
export class FirewallGuard implements CanActivate {
  constructor(private router: Router){}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.checkConnexion()) {
      return true;
    }
    this.router.navigateByUrl('login');
    console.log(window.localStorage.getItem(TOKEN));
  }
  private checkConnexion(): boolean{
    return window.localStorage.getItem(TOKEN) !=null;
  }
}
