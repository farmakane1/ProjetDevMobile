import { TestBed } from '@angular/core/testing';

import { FirewallGuard } from './firewall.guard';

describe('FirewallGuard', () => {
  let guard: FirewallGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(FirewallGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
