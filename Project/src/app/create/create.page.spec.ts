import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { createPage } from './create.page';

describe('createPage', () => {
  let component: createPage;
  let fixture: ComponentFixture<createPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [createPage],
      imports: [IonicModule.forRoot(), ExploreContainerComponentModule]
    }).compileComponents();

    fixture = TestBed.createComponent(createPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
