import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { createPage } from './create.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { createPageRoutingModule } from './create-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    ReactiveFormsModule,
    ExploreContainerComponentModule,
    createPageRoutingModule
  ],
  declarations: [createPage]
})
export class createPageModule {}
