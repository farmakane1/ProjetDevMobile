import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { createPage } from './create.page';

const routes: Routes = [
  {
    path: '',
    component: createPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class createPageRoutingModule {}
