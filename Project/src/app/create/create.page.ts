import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {Salles} from '../models/salle';
import { SalleService } from '../services/salle.service';
import {UtilsService} from '../utils/utils.service';
@Component({
  selector: 'app-create',
  templateUrl: 'create.page.html',
  styleUrls: ['create.page.scss']
})
export class createPage implements OnInit {

  createForm : FormGroup;
  constructor(
    private fb: FormBuilder,
    private router:Router,
    private service:SalleService,
    private utils: UtilsService
  ) { 
    this.createForm = this.fb.group({
     nom: new FormControl('',[Validators.required]),
     description: new FormControl('',[Validators.nullValidator])
    })
  }
  ngOnInit() {
  }
  add(salle:Salles){
    console.log(salle);
    this.service.post(salle).subscribe((salle)=>{
      console.log(salle);
      this.utils.presentToast('Ajouter avec succes','success');
      this.router.navigate(['/tabs/read']);
    },()=>{
        this.utils.presentToast('Veuillez verifier votre saisie','danger');
    })
  }

}
